# PZEM-004T Energy Meter Module Micropython Library For A9 GPRS Module With Micropython Firmware.

## Wiring PZEM-004T with A9 GPRS Module
![Wiring Diagram](https://i.imgur.com/jL7FkEE.png)
## License
[MIT](https://choosealicense.com/licenses/mit/)
